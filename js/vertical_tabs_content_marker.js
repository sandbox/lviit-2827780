(function ($) {

  Drupal.verticalTab.prototype.checkContent = function(inputSelectors, tableSelectors) {

    // Loop all inputs and tables in pane, check for values and break if any found.
    var contentFound = false;

    $(inputSelectors, this.fieldset).each(function() {
      // default values are added as data-attributes to the input wrapper,
      // try to find one and compare it to the input value
      var hasDefaultValue = $(this).closest('.form-wrapper').data('default-value') == $(this).val();

      if ($(this).val() && !hasDefaultValue) {
        contentFound = true;
        return false;
      }
    });

    $(tableSelectors, this.fieldset).each(function() {
      contentFound = true;
      return false;
    });

    // Add marker if pane has content, otherwise remove it
    contentFound ? this.link.addClass('icon--done') : this.link.removeClass('icon--done');
  };

  Drupal.behaviors.vertical_tabs_content_marker = {
    attach: function (context, settings) {
      var inputSelectors = 'input[type=text], textarea, .image-widget input[name="field_imp_header_image[und][0][fid]"]',
          tableSelectors = 'table.ief-entity-table';

      // check for content on load, after ajax requests, and on change to inputs
      $('fieldset.vertical-tabs-pane', '.vertical-tabs-panes').each(function() {
        var tab = $(this).data('verticalTab');
        tab.checkContent(inputSelectors, tableSelectors);
        $(inputSelectors, this).change(function() {
          tab.checkContent(inputSelectors, tableSelectors);
        });
      });
  
    }
  }
})(jQuery);
